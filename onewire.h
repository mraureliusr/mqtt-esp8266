/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <stdint.h>
#include "os_type.h"
#include "gpio.h"

/**
 * @file
 * @brief This is the header file for the one-wire library
 */

/**
 * @struct oneWire_t
 * Structure that holds all the information about onewire connections, including the 64-bit ROM address for each device and some buffer memory to read out the scratchpad
 */
typedef struct {
    uint8_t address[8];
    uint8_t scratchpad[9];
    float temperature;
} oneWire_t;

/**
 * @enum ds18b20_cmds
 * This enum simply gives easy to read names to all the various one-wire commands that the DS18B20 can accept
 */
typedef enum {
    READ_ROM = 0x33,
    MATCH_ROM = 0x55,
    SEARCH_ROM = 0xF0,
    ALARM_SEARCH = 0xEC,
    SKIP_ROM = 0xCC,
    CONVERT_T = 0x44,
    SCRATCH_READ = 0xBE,
    SCRATCH_WRITE = 0x4E,
    SCRATCH_COPY = 0x48,
    E2_RECALL = 0xB8
} ds18b20_cmds;

/**
 * Function sends a reset pulse on the one-wire bus and checks for a presence pulse
 * @return 0 if no presence, 1 if presence
 */
sint8 ICACHE_FLASH_ATTR reset(void);

/**
 * Function which handles all one-wire communication besides the reset sequence.
 * @param owDev A pointer to the oneWire_t variable for this device
 * @param cmd One of the commands from ds18b20_cmds
 */
void ICACHE_FLASH_ATTR transact(oneWire_t *owDev, ds18b20_cmds cmd);
