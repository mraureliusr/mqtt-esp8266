#include <stdint.h>
#include "user_interface.h"
#include "ets_sys.h"
#include "os_type.h"
#include "osapi.h"
#include "mem.h"
#include "espconn.h"
#include "mqtt.h"
#include "onewire.h"
#include "main.h"

os_timer_t tcpTimer;
os_timer_t pingTimer;
os_timer_t pubTimer;

void ICACHE_FLASH_ATTR con(void *arg) {
#ifdef DEBUG
    os_printf("Entered con!\n");
#endif
    mqtt_session_t *pSession = (mqtt_session_t *)arg;
    mqttSend(pSession, NULL, 0, MQTT_MSG_TYPE_CONNECT);

    os_timer_disarm(&pingTimer);
    os_timer_setfn(&pingTimer, (os_timer_func_t *)ping, arg);
    os_timer_arm(&pingTimer, 1000, 0);
}

void ICACHE_FLASH_ATTR pubuint(void *arg) {
#ifdef DEBUG
    os_printf("Entered pubuint!\n");
#endif
    mqtt_session_t *pSession = (mqtt_session_t *)arg;
    uint8_t *data = (uint8_t *)(pSession->userData);
    char *dataStr = os_zalloc(20 * sizeof(char));
    intToStr(*data, dataStr, 4);
    int32_t dataLen = os_strlen(dataStr);
    mqttSend(pSession, (uint8_t *)dataStr, dataLen, MQTT_MSG_TYPE_PUBLISH);
    os_timer_disarm(&pingTimer);
    os_timer_setfn(&pingTimer, (os_timer_func_t *)ping, arg);
    os_timer_arm(&pingTimer, 1000, 0);
}

void ICACHE_FLASH_ATTR pubfloat(void *arg) {
#ifdef DEBUG
    os_printf("Entered pubfloat!\n");
#endif
    mqtt_session_t *pSession = (mqtt_session_t *)arg;
    float *data = (float *)(pSession->userData);
    char *dataStr = os_zalloc(20 * sizeof(char));
    ftoa(*data, dataStr, 2);
    int32_t dataLen = os_strlen(dataStr);
#ifdef DEBUG
    os_printf("Encoded string: %s\tString length: %d\n", dataStr, dataLen);
#endif
    mqttSend(pSession, (uint8_t *)dataStr, dataLen, MQTT_MSG_TYPE_PUBLISH);
    os_timer_disarm(&pingTimer);
    os_timer_setfn(&pingTimer, (os_timer_func_t *)ping, arg);
    os_timer_arm(&pingTimer, 1000, 0);
}

void ICACHE_FLASH_ATTR ping(void *arg) {
#ifdef DEBUG
    os_printf("Entered ping!\n");
#endif
    mqtt_session_t *pSession = (mqtt_session_t *)arg;
    mqttSend(pSession, NULL, 0, MQTT_MSG_TYPE_PINGREQ);
    os_timer_disarm(&pingTimer);
    os_timer_setfn(&pingTimer, (os_timer_func_t *)ping, arg);
    os_timer_arm(&pingTimer, 1000, 0);
}

void ICACHE_FLASH_ATTR sub(void *arg) {
#ifdef DEBUG
    os_printf("Entered sub!\n");
#endif
    mqtt_session_t *pSession = (mqtt_session_t *)arg;
    mqttSend(pSession, NULL, 0, MQTT_MSG_TYPE_SUBSCRIBE);

}

void ICACHE_FLASH_ATTR discon(void *arg) {
#ifdef DEBUG
    os_printf("Entered discon!\n");
#endif
    mqtt_session_t *pSession = (mqtt_session_t *)arg;
    mqttSend(pSession, NULL, 0, MQTT_MSG_TYPE_DISCONNECT);
    os_timer_disarm(&pingTimer);
    os_timer_disarm(&pubTimer);
}

LOCAL void ICACHE_FLASH_ATTR dataLog(void *arg) {
    mqtt_session_t *pSession = (mqtt_session_t *)arg;
    oneWire_t DS18;
    oneWire_t *pDS18 = &DS18;
    uint16_t temp = 0;
    sint32 time = 0;

    pDS18->temperature = 0;
    reset();
    transact(pDS18, SKIP_ROM);
    transact(pDS18, CONVERT_T);
    time = system_get_time() + 750000;
    while(system_get_time() < time);
    reset();
    transact(pDS18, SKIP_ROM);
    transact(pDS18, SCRATCH_READ);
#ifdef DEBUG
    os_printf("\nReceived onewire data: %x %x\n", pDS18->scratchpad[0], pDS18->scratchpad[1]);
#endif
    temp = ((uint16_t)pDS18->scratchpad[1] << 8) | pDS18->scratchpad[0];
    pDS18->temperature += (temp >> 4) + ((temp & 0x0F) * 0.0625);
    pSession->userData = (void *)&pDS18->temperature;
#ifdef DEBUG
    os_printf("\nTemperature is: %d.%02d\n", (int)pDS18->temperature, (int)(pDS18->temperature * 100) % 100);
#endif
    pubfloat(pSession); // publish the temperature
    //discon(pSession);
    return;
}

void ICACHE_FLASH_ATTR user_init() {
    LOCAL mqtt_session_t globalSession;
    LOCAL mqtt_session_t *pGlobalSession = &globalSession;
    char ssid[32] = "yourwifissid";
    char passkey[64] = "yourwifipass";
    struct station_config stationConf;

    gpio_init(); // init gpio so we can use the LED & onewire bus
    uart_div_modify(0, UART_CLK_FREQ / 921600); // set UART to 921600
    wifi_status_led_install(0, PERIPHS_IO_MUX_GPIO0_U, FUNC_GPIO0); // set GPIO0 as status LED
    stationConf.bssid_set = 0;
    os_memcpy(&stationConf.ssid, ssid, 32); // copy the ssid and passkey into the station_config struct
    os_memcpy(&stationConf.password, passkey, 64);
    wifi_set_opmode_current(0x01); //station mode
    wifi_station_set_config_current(&stationConf); // tell it about our config, this auto-connects us as well

    // prepare the TCP/MQTT connection stuff
    // Adafruit IO is at 52.5.238.97
    // use strtoarr.py to generate these
    static const char ioUser[11] = { 0x4d, 0x72, 0x41, 0x75, 0x72, 0x65, 0x6c, 0x69, 0x75, 0x73, 0x52 }; // MrAureliusR
    static const uint8_t ioUser_len = 11;
    static const char ioKey[32] = { }; // use strtoarr.py to generate these
    static const uint8_t ioKey_len = 32;
    static const char ioTopic[27] = { 0x4d, 0x72, 0x41, 0x75, 0x72, 0x65, 0x6c, 0x69, 0x75, 0x73, 0x52, 0x2f, 0x66, 0x65, 0x65, 0x64, 0x73, 0x2f, 0x74, 0x65, 0x73, 0x74, 0x74, 0x6f, 0x70, 0x69, 0x63 }; // MrAureliusR/feeds/testtopic
    static const uint8_t ioTopic_len = 27;
    static const char clientID[5] = { 0x46, 0x52, 0x5a, 0x4e, 0x30 }; // FRZN0
    static const uint8_t clientID_len = 5;
    pGlobalSession->port = 1883; // mqtt port
    static const char adafruitIO_ip[4] = {52, 5, 238, 97};

    // copy all the above info into our mqtt_session_t
    os_memcpy(pGlobalSession->ip, adafruitIO_ip, 4);
    pGlobalSession->username_len = ioUser_len;
    pGlobalSession->username = os_zalloc(sizeof(uint8_t) * pGlobalSession->username_len);
    os_memcpy(pGlobalSession->username, ioUser, pGlobalSession->username_len);
    pGlobalSession->password_len = ioKey_len;
    pGlobalSession->password = os_zalloc(sizeof(uint8_t) * pGlobalSession->password_len);
    os_memcpy(pGlobalSession->password, ioKey, pGlobalSession->password_len);
    pGlobalSession->topic_name_len = ioTopic_len;
    pGlobalSession->topic_name = os_zalloc(sizeof(uint8_t) * pGlobalSession->topic_name_len);
    os_memcpy(pGlobalSession->topic_name, ioTopic, pGlobalSession->topic_name_len);
    pGlobalSession->client_id_len = clientID_len;
    pGlobalSession->client_id = os_zalloc(sizeof(uint8_t) * pGlobalSession->client_id_len);
    os_memcpy(pGlobalSession->client_id, clientID, pGlobalSession->client_id_len);

    // set up all the timers to connect and log data
    os_timer_setfn(&tcpTimer, (os_timer_func_t *)tcpConnect, pGlobalSession);
    os_timer_arm(&tcpTimer, 12000, 0);
    os_timer_setfn(&pingTimer, (os_timer_func_t *)con, pGlobalSession);
    os_timer_arm(&pingTimer, 16000, 0);
    os_timer_setfn(&pubTimer, (os_timer_func_t *)dataLog, pGlobalSession);
    os_timer_arm(&pubTimer, 20000, 1);
}
