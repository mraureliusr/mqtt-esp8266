# MQTT Library for ESP8266 SDK
written by Alexander Rowsell (MrAureliusR)  
[![License](https://img.shields.io/gitlab/license/mraureliusr%2Fmqtt-esp8266?style=plastic&color=orange)](https://www.mozilla.org/en-US/MPL/2.0/)


## Summary
While working on a series of tutorials for Hackaday, I realized MQTT would be the perfect solution for one of the projects I was presenting. However, I didn't want to use a pre-existing MQTT library - that would be boring, and I wouldn't be able to teach the readers as much. Instead, I started to write one from scratch.  

At this point, it's a bit messy. It still needs a lot of work. This documentation will help me keep everything organized and easy-to-use.

The documentation has now been turned into Doxygen documentation. You can view the latest documentation [here](https://mraureliusr.gitlab.io/mqtt-esp8266/index.html)

Make sure to check out the corresponding hackaday.io project!
