#include "os_type.h"

void ICACHE_FLASH_ATTR setupDHT(uint8_t pin);
void ICACHE_FLASH_ATTR getDHTData(uint8_t pin);
