# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

P=main
CC=xtensa-lx106-elf-gcc
AR=xtensa-lx106-elf-ar
LDLIBS=-nostdlib -ggdb -Wl,-Map=output.map -Wl,--start-group -lm -lc -lhal -lpp -llwip -lphy -lnet80211 -lwpa -lat -lwpa2 -lmain -Wl,--end-group -lgcc
CFLAGS= -I. -mlongcalls -std=gnu11 -Wall
LDFLAGS=-Teagle.app.v6.ld
OBJ=main.o mqtt.o onewire.o
DEPS=main.h mqtt.h onewire.h

$(P)-0x00000.bin: $(P)
	esptool.py elf2image $^

$(P): $(OBJ)

%.o: %.c $(DEPS)

flash: $(P)-0x00000.bin
	esptool.py --port /dev/feather0 write_flash 0 $(P)-0x00000.bin 0x10000 $(P)-0x10000.bin

clean:
	rm -f $(P) $(OBJ) $(P)-0x00000.bin $(P)-0x10000.bin *~
