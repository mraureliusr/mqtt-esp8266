#include <stdint.h>
#include "user_interface.h"
#include "os_type.h"
#include "osapi.h"
#include "gpio.h"
#include "dht.h"

#define DHTBus BIT4

LOCAL uint8_t ICACHE_FLASH_ATTR getBitNum(uint32_t bit) {
    uint32_t localBit = bit;
    for(uint8_t i = 0; i <= 16; i++) {
        localBit >>= i;
        if(bit & 0x01) {
            return i;
        }
    }
    return -1; // if we reached this spot we received a bad number
}
void ICACHE_FLASH_ATTR setupDHT(void) {
    gpio_output_set(0, DHTBus, 0, DHTBus); // set up DHTBus as active low output
    ETS_GPIO_INTR_ATTACH(func, NULL);
}

void ICACHE_FLASH_ATTR getDHTData(void) {

}
