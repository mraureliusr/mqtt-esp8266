/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <stdint.h>
#include "os_type.h"
#include "osapi.h"
#include "user_interface.h"
#include "gpio.h"
#include "onewire.h"

#define OWBUS BIT5 /**< This define determines which pin is used */

sint8 ICACHE_FLASH_ATTR reset(void) {
    uint32_t time;
    // reset the ow line, check for presence?
    time = system_get_time() + 500;
    gpio_output_set(0, OWBUS, OWBUS, 0); // pull GPIO4 low
    while(system_get_time() < time); // delay 500uS
    gpio_output_set(0, 0, 0, OWBUS); // let go of GPIO4
    time = system_get_time() + 60;
    while(system_get_time() < time);
    uint8_t presence = (uint8_t)(gpio_input_get() >> 5) & 0x1;
    // give a 480uS pause so the next onewire event doesn't get trampled
    time = system_get_time() + 480;
    while(system_get_time() < time);

    if(!presence) {
        return 1;
    } else {
        return -1;
    }
}

void ICACHE_FLASH_ATTR transact(oneWire_t *owDev, ds18b20_cmds cmd) {
    uint32_t time;
    uint8_t sendBit;
    uint8_t inBit = 0x00;
    uint8_t inByte = 0x00;
    switch(cmd) {
            os_printf("\nOnewire command: %d\n", cmd);
        case SKIP_ROM:
        case CONVERT_T:
            for(uint8_t i = 0; i < 8; i++) {
                sendBit = (cmd >> i) & 0x1;
                //os_printf("\nThe bit is: %d \t i is %d\n", sendBit, i);
                if(sendBit == 1) {
                    //os_printf("\nWe are in sendBit == 1\n");
                    time = system_get_time() + 10;
                    gpio_output_set(0, OWBUS, OWBUS, 0); // pull low
                    while(system_get_time() < time);
                    gpio_output_set(0, 0, 0, OWBUS); // let go
                    time = system_get_time() + 50;
                    while(system_get_time() < time);
                } else {
                    //os_printf("\nWe are in the sendBit else\n");
                    time = system_get_time() + 100;
                    gpio_output_set(0, OWBUS, OWBUS, 0); //pull low
                    while(system_get_time() < time);
                    gpio_output_set(0, 0, 0, OWBUS); // let go
                    time = system_get_time() + 10;
                    while(system_get_time() < time);
                }
            }
            break;
        case SCRATCH_READ:
            for(uint8_t i = 0; i < 8; i++) {
                sendBit = (cmd >> i) & 0x1;
                if(sendBit == 1) {
                    //os_printf("\nWe are in sendBit == 1\n");
                    time = system_get_time() + 10;
                    gpio_output_set(0, OWBUS, OWBUS, 0); // pull low
                    while(system_get_time() < time);
                    gpio_output_set(0, 0, 0, OWBUS); // let go
                    time = system_get_time() + 50;
                    while(system_get_time() < time);
                } else {
                    //os_printf("\nWe are in the sendBit else\n");
                    time = system_get_time() + 100;
                    gpio_output_set(0, OWBUS, OWBUS, 0); //pull low
                    while(system_get_time() < time);
                    gpio_output_set(0, 0, 0, OWBUS); // let go
                    time = system_get_time() + 10;
                    while(system_get_time() < time);
                }
            }
            // now read the scratchpad
            for(uint8_t i = 0; i < 2; i++) {
                for(uint8_t j = 0; j < 8; j++) {
                    time = system_get_time() + 8;
                    gpio_output_set(0, OWBUS, OWBUS, 0);
                    while(system_get_time() < time);
                    gpio_output_set(0, 0, 0, OWBUS);
                    time = system_get_time() + 15;
                    while(system_get_time() < time);
                    inBit = (uint8_t)(gpio_input_get() >> 5) & 0x01;
                    inByte |= inBit << j;
                    time = system_get_time() + 45;
                    while(system_get_time() < time);
                }
                owDev->scratchpad[i] = inByte;
                inByte = 0; // clear inByte
            }
            break;
        default:
            os_printf("\nIncorrect command\n");
            break;
    }
}
